<?php
/**
 * AddressTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * E2E.pt API
 *
 * REST API para integração empresarial com os ERP <a href=\"http://www.primaverabss.com/\" target=\"_blank\">PRIMAVERA</a>, <a href=\"http://www.sage.pt/\" target=\"_blank\">SAGE</a>, <a href=\"https://www.phcsoftware.com/\" target=\"_blank\">PHC</a> e <a href=\"https://www.artsoft.pt/\" target=\"_blank\">ARTSOFT</a>.<br/><br/>Exemplos: <a href=\"https://e2e.pt/examples\" target=\"_blank\">https://e2e.pt/examples</a><br/><br/>Documentação: <a href=\"https://e2e.pt/documentation\" target=\"_blank\">https://e2e.pt/documentation</a><br/><br/>Mais informações, preços e demonstrações em: <a href=\"https://e2e.pt\" target=\"_blank\">https://e2e.pt</a>
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.10
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * AddressTest Class Doc Comment
 *
 * @category    Class
 * @description Address
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AddressTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Address"
     */
    public function testAddress()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "utl_ins"
     */
    public function testPropertyUtlIns()
    {
    }

    /**
     * Test attribute "dt_ins"
     */
    public function testPropertyDtIns()
    {
    }

    /**
     * Test attribute "utl_alt"
     */
    public function testPropertyUtlAlt()
    {
    }

    /**
     * Test attribute "dt_alt"
     */
    public function testPropertyDtAlt()
    {
    }

    /**
     * Test attribute "deleted"
     */
    public function testPropertyDeleted()
    {
    }

    /**
     * Test attribute "extra"
     */
    public function testPropertyExtra()
    {
    }

    /**
     * Test attribute "line1"
     */
    public function testPropertyLine1()
    {
    }

    /**
     * Test attribute "line2"
     */
    public function testPropertyLine2()
    {
    }

    /**
     * Test attribute "local"
     */
    public function testPropertyLocal()
    {
    }

    /**
     * Test attribute "zip"
     */
    public function testPropertyZip()
    {
    }

    /**
     * Test attribute "country"
     */
    public function testPropertyCountry()
    {
    }

    /**
     * Test attribute "account"
     */
    public function testPropertyAccount()
    {
    }
}
