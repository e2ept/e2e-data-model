# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**utl_ins** | **string** |  | [optional] 
**dt_ins** | [**\DateTime**](\DateTime.md) |  | [optional] 
**utl_alt** | **string** |  | [optional] 
**dt_alt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | [optional] 
**extra** | [**\Swagger\Client\Model\Pair[]**](Pair.md) |  | [optional] 
**line1** | **string** |  | [optional] 
**line2** | **string** |  | [optional] 
**local** | **string** |  | [optional] 
**zip** | **string** |  | [optional] 
**country** | [**\Swagger\Client\Model\Country**](Country.md) |  | [optional] 
**account** | [**\Swagger\Client\Model\Account**](Account.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


