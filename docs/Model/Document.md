# Document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**utl_ins** | **string** |  | [optional] 
**dt_ins** | [**\DateTime**](\DateTime.md) |  | [optional] 
**utl_alt** | **string** |  | [optional] 
**dt_alt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | [optional] 
**extra** | [**\Swagger\Client\Model\Pair[]**](Pair.md) |  | [optional] 
**external_id** | **string** |  | [optional] 
**external_source** | **string** |  | [optional] 
**type** | [**\Swagger\Client\Model\DocumentType**](DocumentType.md) |  | [optional] 
**serie** | [**\Swagger\Client\Model\DocumentSerie**](DocumentSerie.md) |  | [optional] 
**number** | **string** |  | [optional] 
**account** | [**\Swagger\Client\Model\Account**](Account.md) |  | [optional] 
**account_billing** | [**\Swagger\Client\Model\Account**](Account.md) |  | [optional] 
**account_delivery** | [**\Swagger\Client\Model\Account**](Account.md) |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**description** | **string** |  | [optional] 
**discount_total** | **float** |  | [optional] 
**vat_total** | **float** |  | [optional] 
**total** | **float** |  | [optional] 
**total_net** | **float** |  | [optional] 
**currency** | **string** |  | [optional] 
**exchange_rate** | **float** |  | [optional] 
**shipping_costs** | **float** |  | [optional] 
**lines** | [**\Swagger\Client\Model\DocumentLine[]**](DocumentLine.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


