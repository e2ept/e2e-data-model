# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**utl_ins** | **string** |  | [optional] 
**dt_ins** | [**\DateTime**](\DateTime.md) |  | [optional] 
**utl_alt** | **string** |  | [optional] 
**dt_alt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | [optional] 
**extra** | [**\Swagger\Client\Model\Pair[]**](Pair.md) |  | [optional] 
**external_id** | **string** |  | [optional] 
**external_source** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**vat_number** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**address** | [**\Swagger\Client\Model\Address[]**](Address.md) |  | [optional] 
**salesman** | [**\Swagger\Client\Model\Salesman[]**](Salesman.md) |  | [optional] 
**balance** | **float** |  | [optional] 
**currency** | **string** |  | [optional] 
**price_type** | [**\Swagger\Client\Model\PriceType**](PriceType.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\Product[]**](Product.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


