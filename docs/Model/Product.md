# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**utl_ins** | **string** |  | [optional] 
**dt_ins** | [**\DateTime**](\DateTime.md) |  | [optional] 
**utl_alt** | **string** |  | [optional] 
**dt_alt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | [optional] 
**extra** | [**\Swagger\Client\Model\Pair[]**](Pair.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\ProductCategory[]**](ProductCategory.md) |  | [optional] 
**unit** | [**\Swagger\Client\Model\Unit**](Unit.md) |  | [optional] 
**accounts** | [**\Swagger\Client\Model\Account[]**](Account.md) |  | [optional] 
**prices** | [**\Swagger\Client\Model\Price[]**](Price.md) |  | [optional] 
**taxes** | [**\Swagger\Client\Model\Tax[]**](Tax.md) |  | [optional] 
**stocks** | [**\Swagger\Client\Model\Stock[]**](Stock.md) |  | [optional] 
**published** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


