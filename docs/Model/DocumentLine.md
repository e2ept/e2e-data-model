# DocumentLine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**utl_ins** | **string** |  | [optional] 
**dt_ins** | [**\DateTime**](\DateTime.md) |  | [optional] 
**utl_alt** | **string** |  | [optional] 
**dt_alt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | [optional] 
**extra** | [**\Swagger\Client\Model\Pair[]**](Pair.md) |  | [optional] 
**document** | [**\Swagger\Client\Model\Document**](Document.md) |  | [optional] 
**line_number** | **int** |  | [optional] 
**product** | [**\Swagger\Client\Model\Product**](Product.md) |  | [optional] 
**quantity** | **float** |  | [optional] 
**unit** | [**\Swagger\Client\Model\Unit**](Unit.md) |  | [optional] 
**price** | **float** |  | [optional] 
**price_total** | **float** |  | [optional] 
**discount** | **float** |  | [optional] 
**discount_total** | **float** |  | [optional] 
**vat** | **float** |  | [optional] 
**vat_total** | **float** |  | [optional] 
**total** | **float** |  | [optional] 
**total_net** | **float** |  | [optional] 
**salesman** | [**\Swagger\Client\Model\Salesman**](Salesman.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


