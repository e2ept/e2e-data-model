# QueryResultDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**columns** | [**\Swagger\Client\Model\Column[]**](Column.md) |  | [optional] 
**records** | [**\Swagger\Client\Model\Record[]**](Record.md) |  | [optional] 
**count** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


